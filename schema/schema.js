const graphql = require("graphql");
const axios = require("axios");
const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLSchema,
    GraphQLList,
    GraphQLNonNull
} = graphql;

const HouseType = new GraphQLObjectType({
    name: "House",
    fields: () => ({
        id: { type: GraphQLString },
        name: { type: GraphQLString },
        description: { type: GraphQLString },
        people: {
            type: new GraphQLList(PersonType),
            resolve(parentValue, args) {
                return axios(
                    `http://localhost:3000/houses/${parentValue.id}/people`
                ).then(res => res.data);
            }
        }
    })
});

const PersonType = new GraphQLObjectType({
    name: "Person",
    fields: () => ({
        // keys are the names of the props the user has
        id: { type: GraphQLString },
        firstName: { type: GraphQLString },
        age: { type: GraphQLInt },
        house: {
            type: HouseType,
            resolve(parentValue, args) {
                console.log(parentValue);
                return axios
                    .get(`http://localhost:3000/houses/${parentValue.houseId}`)
                    .then(res => res.data);
            }
        }
    })
});

const RootQuery = new GraphQLObjectType({
    name: "RootQueryType",
    fields: {
        person: {
            type: PersonType,
            args: { id: { type: GraphQLString } },
            // resolve goes to our database and finds the data
            resolve(parentValue, args) {
                return axios
                    .get(`http://localhost:3000/people/${args.id}`)
                    .then(res => res.data);
            }
        },
        house: {
            type: HouseType,
            args: { id: { type: GraphQLString } },
            resolve(parentValue, args) {
                return axios
                    .get(`http://localhost:3000/houses/${args.id}`)
                    .then(res => res.data);
            }
        }
    }
});
// TODO: Person -> Student?
const mutation = new GraphQLObjectType({
    name: "Mutation",
    fields: {
        addPerson: {
            type: PersonType, // return type, might not always be the same as the type with which we work
            args: {
                firstName: { type: new GraphQLNonNull(GraphQLString) },
                age: { type: new GraphQLNonNull(GraphQLInt) },
                houseId: { type: GraphQLString }
            },
            resolve(parentValue, { firstName, age }) {
                return axios
                    .post(`http://localhost:3000/people`, {
                        firstName,
                        age
                    })
                    .then(res => res.data);
            }
        },
        deletePerson: {
            type: PersonType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parentValue, args) {
                return axios
                    .delete(`http://localhost:3000/people/${args.id}`)
                    .then(res => res.data);
            }
        },
        editPerson: {
            type: PersonType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLString) },
                firstName: { type: GraphQLString },
                age: { type: GraphQLInt },
                houseId: { type: GraphQLString }
            },
            resolve(parentValue, { id, firstName, age, houseId }) {
                return axios
                    .patch(`http://localhost:3000/people/${id}`, {
                        firstName,
                        age,
                        houseId
                    })
                    .then(res => res.data);
            }
        }
    }
});

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation
});
